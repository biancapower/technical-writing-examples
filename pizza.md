# Pizza

A pizza is a savory dish typically made with flattened bread dough and topped with a combination of sauces, meats, vegetables and cheese.

The following is an example of a pizza:

![Pizza on a plate topped with cheese, cherry tomatoes, mushroom and basil leaves](/img/example-pizza-1.jpg)

If you want a tasty and filling meal option, a pizza can be a good choice. Pizzas can be made to suit various dietary preferences, including:

- Vegetarian.
- Vegan.
- Gluten-free.

Pizza may not be a good option however for those looking for a low-carb meal, unless significant substitutions to ingredients are made.