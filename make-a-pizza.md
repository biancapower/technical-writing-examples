# Make a pizza

This guide outlines how to make a vegetarian or non-vegetarian pizza. It does not outline how to make a vegan or gluten-free pizza.

Pizzas are a common family-friendly meal choice in many countries. Learning to make a pizza at home has numerous advantages, including:

- Having complete control over the ingredients.
- Saving money compared to ordering pizza from a restaurant.
- Avoiding the need to get dressed to accept delivery of a pizza.

The following flow chart outlines the process of making a pizza at a high level. More detail is provided in subsequent sections.

```mermaid
graph TD
    A[START] --> B{Has access to necessary<br> equipment}
    B -->|Yes| C{Has access to basic<br> ingredients}
		B -->|No| D{Is able to obtain access<br> to necessary equipment}
		D -->|Yes| E(Obtain access to<br> necessary equipment)
		D -->|No| F([NO PIZZA])
		E --> C
		C -->|No| G{Is able to obtain access<br> to basic ingredients}
		G -->|Yes| H(Obtain access to basic<br> ingredients)
		H --> I
		G --> |No| F
		C -->|Yes| I{Is vegetarian}
		I -->|Yes| N(Turn on oven)
		I -->|No| J{Has access to<br> non-vegetarian<br> ingredients}
		J -->|Yes| N
		J -->|No| K{Is able to obtain<br> access to<br> non-vegetarian<br> ingredients}
		K -->|No| L{Willing to convert to<br> vegetarianism}
		L -->|No| F
		L -->|Yes| I
		K -->|Yes| M(Obtain access to<br> non-vegetarian ingredients)
		M --> N
		N --> O(Prepare toppings)
		O --> P(Place pizza base on pizza tray)
		P --> Q(Apply pizza sauce to pizza base)
		Q --> R{Is Vegetarian}
		R -->|Yes| T(Add mushrooms)
		R -->|No| S(Add non-vegetarian toppings)
		S --> T
		T --> U(Add fresh basil leaves)
		U --> V(Add shredded cheese)
		V --> W(Add cherry tomatoes)
		W --> X(Put pizza in oven)
		X --> Y(Allow appropriate time to pass)
		Y --> Z(Remove pizza from oven)
		Z --> AA{Is cooked}
		AA -->|No| X
		AA -->|Yes| AB([fa:fa-pizza PIZZA IS MADE])
```

## Prerequisites

Before continuing make sure you have access to all necessary equipment and desired ingredients. Necessary equipment and suggested ingredients are listed below.

### Equipment

- Pizza tray.
- Oven.
- Kitchen knife.
- Chopping board.
- Large tablespoon.

### Basic ingredients

- 1 pre-made pizza base.
- 100g pizza sauce.
- 200g shredded cheese.
- 50g cherry tomatoes.
- Small handfull of fresh basil leaves.
- 50g mushrooms.

#### Non-vegetarian toppings

Your choice of meats, which may include:
- Ham.
- Salami.
- Pepperoni.

## Making the pizza

Once all necessary equipment and ingredients have been obtained, we can begin making the pizza.

To make the pizza:

1. Turn on the oven to 200°C.
1. Prepare toppings:
	1. Place cherry tomatoes on chopping board.
	1. Using the kitchen knife, slice all cherry tomatoes in half.
	1. If including non-vegetarian ingredients:
		1. Place meats on chopping board to the side of tomatoes.
		1. Use the kitchen knife to chop the meats into small square pieces.
1. Place pizza base on pizza tray.
1. Squirt pizza sauce into centre of pizza base.
1. Use back of table spoon to spread pizza sauce evenly across pizza base.
1. If not vegetarian, scatter meats across base of pizza, on top of pizza sauce. If vegetarian, you may skip this step.
1. Scatter remaining ingredients across top of pizza in the following order, taking care to distribute each ingredient as evenly as possible across the surface area of the pizza:
	1. Mushrooms.
	1. Fresh basil leaves.
	1. Shredded cheese.
	1. Cherry tomatoes.
1. Open door of oven.
1. Slide pizza tray containing pizza into oven.
1. Close door of oven.
1. Wait 15 minutes.
1. Open oven door.
1. Remove pizza from oven.
1. If the pizza is not cooked return it to the oven for a further 5 minutes, then remove from oven.
1. If the pizza is cooked, turn off the oven.

When the pizza is cooked, we are done.