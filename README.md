# Technical writing examples

This repo contains demonstrations of my technical writing. To prepare for creating these I first completed both Google's [Technical Writing One](https://developers.google.com/tech-writing/one/) course and GitLab's [Technical Writing Fundamentals](https://about.gitlab.com/handbook/engineering/ux/technical-writing/fundamentals/). I also referred to [GitLab's Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide/index.html) while writing these examples.

Technical writing examples in this repo:

- Concept: [Pizza](pizza.md)
- Task / Procedure: [Make a pizza](make-a-pizza.md)